# net.lisias.whistleblower

A small, lightweight, slow =P but cheap way to lift some load from your servers by black listing some bastards. ;-)

## Introduction

Some years ago, about 2008 or 2009 I think, I came to the idea to lift up my own server. http://www.lisias.net was born.

Being a dirty cheap counting beans kind of person :-D, I choose to use my own distro on the cheapest VPS provider I found. (They were cheap, but they were attentive and competent - I miss that guys!).

Well, reality knocked out in a week - my server was sustaining a heavy load of requests. Surprised? I should not be. :-D

Well, being also a very paranoid developer (being cheap and being paranoic is almost a schizophrenic symptom! =P ) , instead of buying support to do things I don't understand, I resolved to study the problem - and being on forced vacations =P helped a lot.

I learnt how to monitor every single log from a Linux box. And how to add some more. And then I wrote my own reports from that logs in bash, grep, sed and awk. Looking on them now, I'm not that proud about the quality of the work but they did the job.

Now I'm using mainly Perl as a workhorse for the report building. Weird syntax, but it's a well supported, sturdy and lighting fast tool.

Now, with some [projects](http://retro.lisias.net/my/projects/network/) being released into the wild, I think it could be a good idea to publish this thing.


## Server Side

The scripts are on my daily cron, and they parse every interesting log to consolidate the *interesting* bits of data. Then they parse that huge bunch of data to emit the reports I want.

I currently rewriting these things (seriously, my initial approach was effective, but horribly inefficient) and sooner or later they will be published.

Currently, the reports are available at [report.lisias.net/](http://report.lisias.net/)

Read that page carefully. By no means this is a professional report, I have no way to probe if the *interesting accesses* are really attacks or just a script kiddie that don't know what they are doing.

The bits of the report that should interest you is the "attacks". 

The PAM attacks monitoring are deactivated, I closed that door definitely and I was left without anything interesting to monitor. :-)

(you can DDoS' me, but you will not intrude my box by PAM. Point.)

The [HTTP attacks](http://report.lisias.net/attacks/http/) are the ones that worth a visit. By past experience, I learnt that the HTTP attackers invariably also give a try on PAM, so the lost of the PAM monitoring didn't made much harm.

(However, it was useful to double check the data - if an "http attacker" was not also mentioned by the PAM report, chances are that it was a false positive - without this double check, please take that reports with a nice grain of salt).


## Client Side

Well, what you do with these data is not my problem :-) but I have some (nice) suggestions about what to do with them.

Read more [here](clients/).


Known Clients
-------------

* [net.lisias.retro Micro Services](https://bitbucket.org/Lisias/net.lisias.retro.pub).


# WARNING

Please be advised that currently there's no white lists on my servers, I'm painting black everybody that hits my servers too much.

For example, right now `crawl-66-249-75-186.googlebot.com` is being listed on my reports and perhaps this is a guy that you would not want to block on your production servers.