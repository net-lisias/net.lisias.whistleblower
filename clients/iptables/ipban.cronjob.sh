#!/usr/bin/env bash

chain_exists()
{
	[ $# -lt 1 -o $# -gt 2 ] && {
		echo "Usage: chain_exists <chain_name> [table]" >&2
		return 1
	}
	local chain_name="$1" ; shift
	[ $# -eq 1 ] && local table="--table $1"
	iptables $table -n --list "$chain_name" >/dev/null 2>&1
}

chain_create()
{
	[ $# -lt 1 -o $# -gt 2 ] && {
		echo "Usage: chain_create <chain_name> [table]" >&2
		return 1
	}
	local chain_name="$1" ; shift
	[ $# -eq 1 ] && local table="--table $1"
	iptables $table -N "$chain_name" >/dev/null 2>&1
	iptables $table -t filter -A INPUT -j "$chain_name"
}

chain_flush()
{
	[ $# -lt 1 -o $# -gt 2 ] && {
		echo "Usage: chain_create <chain_name> [table]" >&2
		return 1
	}
	local chain_name="$1" ; shift
	[ $# -eq 1 ] && local table="--table $1"
	iptables --flush INPUT.block.temp
	iptables -A INPUT.block.temp -j RETURN
} >/dev/null 2>&1


banningips_populate()
{
	BANNING_IPS=[]
	BAD_GUYS_IP=[]

	local IP_BY_HIT=$(curl http://report.lisias.net/attacks/http/report.sources.by_hits.txt)
	IFS=$'\n'
	for line in $IP_BY_HIT
	do
		IFS=$'\t' read hit ip <<< "$line"

		# RULE: Ban everyone that hit us 1000 times or more. By this time, they're hopeless.
		if [ ${hit#0} -gt 1000 ] ; then
			BANNING_IPS+=($ip)
		else
			# We don't need to waste memory and processing remembering who have more than 100 hits because we will ban them unconditionally
			if [ ${hit#0} -gt 10 ] ; then
				BAD_GUYS_IP+=($ip)
			fi
		fi
	done
	unset IP_BY_HIT

	local IP_BY_PERIOD=$(curl http://report.lisias.net/attacks/http/report.sources.last_week.txt)
	for ip in $IP_BY_PERIOD
	do
		# RULE: If the bastard hit us 10 times or more, and did it again in the week, he's banned for today.
		if [[ " ${BAD_GUYS_IP[@]} " =~ " ${ip} " ]] ; then
			local ignore=it
		else
			BANNING_IPS+=($ip)
		fi
	done
	unset IP_BY_PERIOD
} >/dev/null 2>&1

banningips_execute()
{
	for ip in ${BANNING_IPS[@]}
	do
		echo $1 - $ip
		iptables -I $1 1 -s $ip -j DROP
	done
} >/dev/null 2>&1

run() {
	chain_exists INPUT.block.temp || chain_create INPUT.block.temp
	banningips_populate
	chain_flush INPUT.block.temp
	banningips_execute INPUT.block.temp

	# Log the banned IPs on /var/log/ipban.log
	iptables -L INPUT.block.temp > /var/log/ipban.log
}

run
