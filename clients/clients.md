# net.lisias.whistleblower Clients

## Introduction

Data by itself is useless. Knowledge should have a purpose.

The is the purpose of my efforts on monitoring my servers and publishing the reports.

All that unwanted traffic is harmfull. You waste bandwidth, you waste processing power - and on nowadays VPS, it means these bastards are corroding your quota, effetivelly using your money to make their living.

Worst, such bastards make the home enthusiast unwilling to make things and serve them. The *Maker* scene ends up being a scarcelly habited land where only the bravest, the smartest and the richest can play.

Ok, that was dramatic. :-)

On the bottom line, there's a lot of small applicances that could be used to serve interesting things cheaply on the Internet - **that** was the purpose of that Thing after all!


## Clients

There's no safer place for a ship that a port - but we don't build ships to be in ports, right? So you can't just block everybody without blocking yourself from doing what you want.

By using my reports to block the most nasty requests, your small appliances are less punished by them and so they can be used to do your interesting stuff instead of being forgotten in a closet because they can't sustain the heat. You will not be blocked from all the nasty dangers of the Wild, Wide Web - but I intent to help you to survive most of them. :-)

These tools are field proven, I have a small [appliance](http://home.lisias.net:8080) right know being abused... I mean =P ... being used to serve some interesting services.

The poor thing is a Raspberry PI 2011.12 Model B, and it's holding well. :-)


### [iptbles tooling](iptables/)

The fast way to get rid of that pesky requests is by banning the IP on iptables. Simple like that.

These are scripts using iptables as a tool. You can access them [here](iptables/).

Obviously, you must have a [iptables](https://en.wikipedia.org/wiki/Iptables) capable box.


#### ipban.cronjob.sh

A cronjob intented to be soft linked into your `/etc/cron.daily/`. Do it and forget about. :-)

The script fetchs data from [report](http://report.lisias.net/attacks/http/) and build a list of IPs that should be banned. Simple. Practical. Unfair. =D

The banning rules are simple:

* Every IP with 100 hits or more from all time are black listed. Point.
* Every IP with less than 100 hits but more than 10 that hit my server in the last week are black listed.

A chain called `INPUT.block.temp` is created to handle them. The chain is flushed and repopulated daily - you will be vulnerable in the few seconds needed to repopulate the chain. The best approach would be creating a new chain, and then switch them before destroying the old - I will fix this someday.

The list of banned IPs will be available on `/var/log/ipban.log` for conference.

Please be advised that currently there's no white lists on my reports, I'm painting black everybody that hits my servers too much. For example, right now `crawl-66-249-75-186.googlebot.com` is being blocked by these rules, what you may not want to do on a production machine.



